/************************
*	Hina Nadir	*
*	05650		*
*	BESE-4B		*
*************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

typedef void (*vmethod) ();

struct matrix{
	int no_of_rows;
	int no_of_cols;
	int *A;
	void (*ptr_to_mult)(struct matrix *X, struct matrix *B);
	void (*ptr_to_add)(struct matrix *X, struct matrix *B);
	void (*ptr_to_norm)(struct matrix *X);
	vmethod *vtable_ptr;	
};

struct vector{
	struct matrix base;
	int max_elem;
	int min_elem;	
};


void mmult(struct matrix *X, struct matrix *B){
	
	int *result = malloc(sizeof(int) * X->no_of_rows * B->no_of_cols);

	int i,j,k;
	for(i=0; i<X->no_of_rows; i++){
		for(j=0; j<B->no_of_cols; j++){
			for(k=0; k<B->no_of_rows; k++){
			result[i * B->no_of_rows + j] += 
				X->A[i * X->no_of_cols +k] * B->A[k * B->no_of_cols + j]; 				}			
		}
	}

	for(i=0; i < (X->no_of_rows * X->no_of_cols); i++)
		printf("%d\t", X->A[i]);

	for(i=0; i < (B->no_of_rows * B->no_of_cols); i++)
		printf("%d\t", B->A[i]);

	for(i=0; i < 3//(X->no_of_rows * B->no_of_cols)
; i++)
		printf("%d\t", result[i]);
	

}

void madd(struct matrix *X, struct matrix *B){

	int *result = malloc(sizeof(int) * X->no_of_rows * B->no_of_cols);
	int i;
	for(i=0; i< X->no_of_rows * X->no_of_cols; i++){
		result[i] = X->A[i] + B->A[i];
		printf("%d",result[i]);
	}
	
}

void vadd(struct matrix *X, struct matrix *B){

	int *result = malloc(sizeof(int) * X->no_of_rows * B->no_of_cols);
	int i;
	for(i=0; i< X->no_of_rows * X->no_of_cols; i++){
		result[i] = X->A[i] + B->A[i];
		printf("%d",result[i]);
	}
}

void mnorm(struct matrix *X){
	int *result = malloc(sizeof(int) * X->no_of_cols);

	int i,j;
	for(i=0; i < X->no_of_cols; i++){
		for(j=0; j < X->no_of_rows; j++){
			result[j] += abs(X->A[j]); 
		}
	}

	int max = result[0];
	for(i=1; i< X->no_of_cols; i++){
		if(result[i]>max)
			max=result[i];
	}

	printf("norm: %d\n",max);
}

void vnorm(struct matrix *v){

	int i,norm=0;
	for(i=0; i<v->no_of_rows;i++)
		norm += abs(v->A[i]);

	printf("vnorm: %d\n",norm);

}

void m_init(struct matrix *m){

	m->no_of_rows=2;
	m->no_of_cols=2;

	m->A = (int*)malloc(sizeof(int) * m->no_of_rows * m->no_of_cols);

	int i;
	for(i=0; i < (m->no_of_rows * m->no_of_cols); i++)
		m->A[i] =4; 

	m->ptr_to_mult = &mmult;
	m->ptr_to_add = &madd;
	if(*m->vtable_ptr == &mnorm){
		m->ptr_to_norm = &mnorm;
	}
	else
	{
		m->ptr_to_norm = &vnorm;
	}
}

void v_init(struct vector *v){
	v->base.no_of_rows=2;
	v->base.no_of_cols=1;
	v->base.A = (int*)malloc(sizeof(int) * v->base.no_of_rows * v->base.no_of_cols);

	int i;
	for(i=0; i < (v->base.no_of_rows * v->base.no_of_cols); i++)
		v->base.A[i] =2;

	v->max_elem=2;
	v->min_elem=2;

	v->base.ptr_to_mult = &mmult;
	v->base.ptr_to_add = &vadd;
	v->base.ptr_to_norm = &vnorm;
}

int main(){

	vmethod *mvtable = malloc(sizeof(vmethod));
	mvtable[0] = &mnorm;
	vmethod *vvtable = malloc(sizeof(vmethod));
	vvtable[0] = &vnorm;

	struct matrix *m = (struct matrix *)(malloc(sizeof(struct matrix)));
	m->vtable_ptr = &mvtable[0];
	m_init(m);
	m->ptr_to_norm(m);

	//vtable stuff
	struct matrix *mv = (struct matrix *)(malloc(sizeof(struct vector)));
	mv->vtable_ptr = &vvtable[0];
	m_init(mv);
	mv->ptr_to_norm(mv);

	struct vector *v = (struct vector *)(malloc(sizeof(struct vector)));
	v->base.vtable_ptr = &vvtable[0];
	v_init(v);
	v->base.ptr_to_norm(v);
	
	return 0;
}
